<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    use HasFactory;
    protected $fillable = [
        'date',
        'description',
        'isGlobal',
        'location_id'
    ];

    protected $dates = ['date'];

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

public function getHolidaysByLocation($locationId)
    {
        //dd($locationId);
        $holi = Holiday::where('location_id', $locationId)->get();
        //dd($holi);
        return $holi;

    }


}
