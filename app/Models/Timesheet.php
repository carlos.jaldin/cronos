<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\sortHelper;

class Timesheet extends Model
{
    use HasFactory;

    protected $fillable = [
        'checkIn',
        'checkOut',
        'date',
        'schedule_status_id',
        'team_member_id'
    ];

    protected $dates = ['date'];

    public function scheduleStatus(){
        return $this->belongsTo(ScheduleStatus::class);
    }

    public function teamMember(){
        return $this->belongsTo(TeamMember::class);
    }

    public function getCheckInCheckOut($user, $team, $date){
        $member = TeamMember::where('user_id',$user)->where('team_id',$team)->first();
        //dd($member);
        //existe una tupla en del miembro de esta fecha? o si existe una tupla que al menos tenga el checkout lleno
        if($member == null){
            return [
                'error' => 'tam member does not exits'
            ];
        }
        //dd($user,$team,$date); yyyy-mm-dd
        $checks = Timesheet::where('date', $date)
                    ->where('team_member_id',$member->id)
                    ->orderBy('id', 'desc')
                    ->first(); //"asd" ["asd","dd"]
        return array(
            'new' => ($checks != null && $checks->checkOut != null && $checks->checkIn != null), //mas de la primera vez
            'checkOut' => ($checks!=null && $checks->checkOut == null), // hay un checkout
            'checkIn' => ($checks == null), //primera vez del timesheet para un member
            'checks' => $checks
        );
    }

    public function createTimeSheet($date,$checkIn=null,$checkOut=null,$teamMemberId,$scheduleStatus) {
		return Timesheet::create([
            'checkIn' => $checkIn,
            'checkOut' => $checkOut,
            'date' => $date,
			'schedule_status_id' => $scheduleStatus,
			'team_member_id' => $teamMemberId
        ]);
	}

    public function setCheckOut($timesheet, $checkOut){
        $ts =  Timesheet::findOrFail($timesheet); //Timesheet::where('id',$timesheet)->get()
        $ts->checkOut = $checkOut;
        $ts->update();
    }

	public function getTimeSheetsByDay($userId, $date) {
		$response = Timesheet::select(
			'timesheets.date AS date',
			'timesheets.checkIn AS checkIn',
			'timesheets.checkOut AS checkOut',
			'users.name AS userName',
			'users.id AS userId',
			'team_members.user_id AS memberId',
			'roles.name AS role',
			'teams.name AS teamName',
			'schedule_statuses.status AS timesheetstatus'
		)
			->join('team_members', 'timesheets.team_member_id', '=', 'team_members.id')
			->join('teams', 'team_members.team_id', '=', 'teams.id')
			->join('users', 'team_members.user_id', '=', 'users.id')
			->join('roles', 'team_members.role_id', '=', 'roles.id')
			->join('schedule_statuses', 'timesheets.schedule_status_id', '=', 'schedule_statuses.id')
			->where('timesheets.date', $date)
			->where('users.id', $userId)
			->get()->toArray();

		return $response;
	}

	public function getUsersByDate($date1, $date2) {
		return $this->getAllUsersByDate($date1, $date2);
	}

	public function getUserAttendanceHistoryByMonth($userId, $month) {
		$response = [];
		$users = $this->getAllUsersByDate($month);
		if (sizeof($users) > 0) {
			$response = array_filter($users, function($user) use($userId) {
				return $user['userid'] == $userId;
			});
		}

		return $response;
	}

    private function getAllUsersByDate($date1, $date2=null)
    {
		$users = $this->getUsersTimeSheetQuery($date1, $date2);

        $result = [];
        if (sizeof($users) == 0) {
            return [];
        }

        $result[] = array(
			'userid' => $users[0]['member'],
            'username' => $users[0]['userName'],
            'role' => $users[0]['role'],
            'location' => $users[0]['location'],
            'team' => $users[0]['teamName'],
            'date' => Carbon::parse($users[0]['date'])->format('Y-m-d'),
            'hours' => 0,
			'timesheetstatus' => $users[0]['timesheetstatus'],
        );

        $reference = $users[0];

        foreach ($users as $user) {

            if ($reference['member'] == $user['member'] && $reference['date'] == $user['date']) {
				if ($user['checkOut'] && $user['checkIn']) {
					$to = Carbon::createFromFormat('H:s:i', $user['checkOut']);
					$from = Carbon::createFromFormat('H:s:i', $user['checkIn']);
					$result[sizeof($result) - 1]['hours'] += $to->diffInHours($from);
				}
            } else {
				$timeDifference = 0;
				if ($user['checkOut'] && $user['checkIn']) {
					$to = Carbon::createFromFormat('H:s:i', $user['checkOut']);
                	$from = Carbon::createFromFormat('H:s:i', $user['checkIn']);
					$timeDifference = $to->diffInHours($from);
				}
                $result[] = array(
					'userid' => $user['member'],
                    'username' => $user['userName'],
                    'role' => $user['role'],
                    'location' => $user['location'],
                    'team' => $user['teamName'],
                    'date' => Carbon::parse($user['date'])->format('Y-m-d'),
                    'hours' => $timeDifference,
					'timesheetstatus' => $user['timesheetstatus'],
                );

                $reference = $user;
            }
        }
		// $control = 'date';
        // $sortedList = (new sortHelper)->merge_sort($result,$control);
        // dd($sortedList);

        return $result;
    }

	private function getUsersTimeSheetQuery($date1, $date2=null) {
		$response = [];
		if ($date2 == null) {
			$response = Timesheet::select(
				'timesheets.date AS date',
				'timesheets.checkIn AS checkIn',
				'timesheets.checkOut AS checkOut',
				'users.name AS userName',
				'users.id AS member',
				'roles.name AS role',
				'locations.abbreviation AS location',
				'teams.name AS teamName',
				'schedule_statuses.status AS timesheetstatus'
			)
				->join('team_members', 'timesheets.team_member_id', '=', 'team_members.id')
				->join('teams', 'team_members.team_id', '=', 'teams.id')
				->join('users', 'team_members.id', '=', 'users.id')
				->join('roles', 'team_members.role_id', '=', 'roles.id')
				->join('locations', 'users.location_id', '=', 'locations.id')
				->join('schedule_statuses', 'timesheets.schedule_status_id', '=', 'schedule_statuses.id')
				->whereMonth('timesheets.date', $date1->month)
				->whereYear('timesheets.date', $date1->year)
				->orderBy('member', 'ASC')->orderBy('date', 'ASC')
				->get()->toArray();
		} else {
			$response = Timesheet::select(
				'timesheets.date AS date',
				'timesheets.checkIn AS checkIn',
				'timesheets.checkOut AS checkOut',
				'users.name AS userName',
				'users.id AS member',
				'roles.name AS role',
				'locations.abbreviation AS location',
				'teams.name AS teamName',
				'schedule_statuses.status AS timesheetstatus'
			)
				->join('team_members', 'timesheets.team_member_id', '=', 'team_members.id')
				->join('teams', 'team_members.team_id', '=', 'teams.id')
				->join('users', 'team_members.id', '=', 'users.id')
				->join('roles', 'team_members.role_id', '=', 'roles.id')
				->join('locations', 'users.location_id', '=', 'locations.id')
				->join('schedule_statuses', 'timesheets.schedule_status_id', '=', 'schedule_statuses.id')
				->whereBetween('timesheets.date', [$date1, $date2])
				->orderBy('member', 'ASC')->orderBy('date', 'ASC')
				->get()->toArray();
		}

		return $response;
	}
}
