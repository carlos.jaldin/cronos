<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduleStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'status'
    ];
    
    public function timesheets(){
        return $this->hasMany(Timesheet::class);
    }

}
