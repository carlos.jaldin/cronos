<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $fillable = [
        'abbreviation',
        'name'
    ];
    
    //protected $dates = ['date'];

    public function holidays(){
        return $this->hasMany(Holiday::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }

    public function getOnlyCities(){
        return Location::where('name','!=','Bolivia')->get();
    }
}

//$user->location = 1 
// $user->load('location');
// $user->location->name