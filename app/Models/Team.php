<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

class Team extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'name'
    ];

    public function teamMembers(){
        return $this->hasMany(TeamMember::class);
    }

    public function getTeamMembers($id){
        $members = User::whereIn('id',TeamMember::select('user_id')->where('team_id', $id)->get())->get();
        $members->load('location');
        return $members;
    }

    public function getTeamFromId($id){
        return Team::findOrFail($id);
    }

    public function isLeaderOfTeam($team, $user){
        return (TeamMember::
                where('user_id', $user)
                ->where('team_id', $team)
                ->where('isLeader', true)
                ->first() != null);
    }
}
