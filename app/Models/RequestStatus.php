<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'status'
    ];

    public function vacationRequests(){
        return $this->hasMany(VacationRequest::class);
    }

    public function allExceptOnHold(){
        return RequestStatus::where('id','!=',3)->get();
    }
    //protected $dates = ['date'];

}
