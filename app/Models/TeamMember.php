<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
    use HasFactory;

    protected $fillable = [
        'isActive',
        'isLeader',
        'initDate',
        'endDate',
        'team_id',
        'user_id',
        'role_id',
    ];

    protected $dates = ['initDate', 'endDate'];

    public function timesheets(){
        return $this->hasMany(Timesheet::class);
    }

    public function vacations(){
        return $this->hasMany(VacationRequest::class);
    }

    public function team(){
        return $this->belongsTo(Team::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function getTeamMember($user, $team){
        return TeamMember::where('user_id', $user)->where('team_id', $team)->first();
    }


}
