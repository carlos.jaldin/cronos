<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacationRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'response',
        'start',
        'end',
        'viewed',
        'request_statuses_id',
        'team_member_id'
    ];

    protected $dates = ['start', 'end'];

    public function teamMember(){
        return $this->belongsTo(TeamMember::class);
    }

    public function requestStatuses(){
        return $this->belongsTo(RequestStatus::class);
    }

    public function createVacationRequest($start, $end, $status, $member){
        return VacationRequest::create([
			'response' => null,
            'start' => $start,
            'end' => $end,
            'request_statuses_id' => $status,
            'team_member_id' => $member
        ]);
    }

	public function getRequestVacationsByTeamMemberId($teamMemberId) {
		return VacationRequest::where('team_member_id', $teamMemberId)->get();
	}

    public function getRequestVacationsOfTeam($team){
        $teamMembers = TeamMember::select('id')->where('team_id', $team)->get();
        $requests =  VacationRequest::whereIn('team_member_id',$teamMembers)
                ->where('request_statuses_id', 3)->orderby('id','desc')->get()
                ->load('requestStatuses')->load('teamMember');
        foreach ($requests as $request){
            $request->teamMember->load('user');
        }
        return $requests;
    }

	public function getRequestVacationById($requestVacationId) {
		return VacationRequest::find($requestVacationId)->load('teamMember');
	}

    public function markAsView($requestVacationId){
        $reqvac = VacationRequest::findOrFail($requestVacationId);
        $reqvac->viewed = true;
        $reqvac->update();
    }

    public function setRequestStatus($requestVacationId, $response, $status){
        $data = VacationRequest::findOrFail($requestVacationId);
        $data->viewed = true;
        $data->response = $response;
        $data->request_statuses_id = $status;
        $data->update();
    }
}
