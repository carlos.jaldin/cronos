<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function teamMembers(){
        return $this->hasMany(TeamMember::class);
    }

    public function getRole($id){
        $holi = Holiday::where('user_id', $id)->get();
        return $holi;
    }

}
