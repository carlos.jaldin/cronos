<?php

namespace App\Http\Services;
use App\Models\User;
use Illuminate\Http\Request;

//use App\Models\Location;
class UserService
{
    private User $user;
    //private Location $location;
    public function __construct(){
        $this->user = new User();
        //$this->location = new Location();
    }

    public function listUser()
    {
        try{
            return $this->user->listUser();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getTeamsOfUser($id){
        try{
            return $this->user->getTeamsOfUser($id);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

}
