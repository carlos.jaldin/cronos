<?php

namespace App\Http\Services;

use App\Models\Team;
use App\Models\TeamMember;
use Illuminate\Support\Facades\Auth;

class TeamMemberService
{
    private TeamMember $teamMember;

    public function __construct()
    {
        $this->teamMember = new TeamMember();
    }

    public function getTeamMember($team){
        return $this->teamMember->getTeamMember(Auth::user()->id, $team);
    }
}
