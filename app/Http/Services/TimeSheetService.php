<?php

namespace App\Http\Services;

use App\Models\TeamMember;
use App\Models\Timesheet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TimesheetService
{
    private Timesheet $timesheet;
    private TeamMember $teamMember;
    public function __construct(){
        $this->timesheet = new Timesheet();
        $this->teamMember = new TeamMember();
    }

    public function setTimesheetsByLeader($startDate, $endDate, $teamMemberId, $scheduleStatus){
        $currentDate = $startDate;
        $finalDate = $endDate;
        do{
            $currentDate->addDay();
            $this->timesheet->createTimeSheet($currentDate, null, null, $teamMemberId, $scheduleStatus);
        }while(!$currentDate->equalTo($finalDate));
    }

    public function setCheckIn($team){
        $date = Carbon::now('America/La_Paz');
        $teamMember = $this->teamMember->getTeamMember(Auth::user()->id, $team);
        return $this->timesheet->createTimeSheet($date->format('Y-m-d'), $date->format('H:i:s'), null, $teamMember->id, 4);
    }

    public function setCheckOut($timesheet){
        $date = Carbon::now('America/La_Paz');
        return $this->timesheet->setCheckOut($timesheet,$date->format('H:i:s'));
    }

    public function getCheckInCheckOut($team){
        try {
            $date = Carbon::now('America/La_Paz');
            //dd($date->format('H:i'));
            // dd($date->format('m-d')); //shows only month and day like string "08-11"
            // dd($date->format('H:i')); //shows only hour and minute like string "16:29"
            //$timesheet->date->format('m-d') == $otro->date->format('m-d');
            return $this->timesheet->getCheckInCheckOut(Auth::user()->id, $team, $date->format('Y-m-d'));
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function showBetweenDates($date1,$date2){
        try{
            return $this->timesheet->getUsersByDate($date1,$date2);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

	public function getUserAttendanceHistoryByMonth($userId, $month) {
		try{
            return $this->timesheet->getUserAttendanceHistoryByMonth($userId, $month);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
	}

	public function getTimeSheetsByDay($userId, $date) {
		try{
            return $this->timesheet->getTimeSheetsByDay($userId, $date);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
	}
}
