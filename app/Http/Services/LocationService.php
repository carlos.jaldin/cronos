<?php

namespace App\Http\Services;
use App\Models\Location;
class LocationService
{
    private Location $locationModel;
    public function __construct(){
        $this->locationModel = new Location();
    }

    public function getOnlyCities()
    {
        try{
            return $this->locationModel->getOnlyCities();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
