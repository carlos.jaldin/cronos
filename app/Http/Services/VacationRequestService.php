<?php

namespace App\Http\Services;
use App\Models\RequestStatus;
use App\Models\TeamMember;
use App\Models\VacationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VacationRequestService
{
    private VacationRequest $vacationRequestModel;
    private RequestStatus $request;
    private TeamMember $teamMember;
    private TimesheetService $timeSheetService;
    public function __construct(){
        $this->vacationRequestModel = new VacationRequest();
        $this->teamMember = new TeamMember();
        $this->request = new RequestStatus();
        $this->timeSheetService = new TimesheetService();
    }

    public function createVacationRequest(Request $request, $team){
        Request()->validate([
            'start' => ['required'],
            'end' => ['required', 'after:start']
        ]);
        $member = $this->teamMember->getTeamMember(Auth::user()->id,$team);
        return $this->vacationRequestModel->createVacationRequest($request->get('start'), $request->get('end'), 3, $member->id);
    }

	public function getUserVacationRequests($teamId) {
		$teamMember = $this->teamMember->getTeamMember(Auth::user()->id, $teamId);
		return $this->vacationRequestModel->getRequestVacationsByTeamMemberId($teamMember->id);
	}

	public function getVacationRequestById($vacationRequestId) {
		return $this->vacationRequestModel->getRequestVacationById($vacationRequestId);
	}

    public function getRequestVacationsOfTeam($team){
        return $this->vacationRequestModel->getRequestVacationsOfTeam($team);
    }

    public function allExceptOnHold(){
        return $this->request->allExceptOnHold();
    }

    public function manageResponse(Request $request){
        Request()->validate([
            'request_statuses_id' => ['required'],
            'response' => ['required'],
            'request_vacation_id' => ['required'],
            'team_member_id' => ['required'],
            'team_id' => ['required']
        ]);
        $this->vacationRequestModel->setRequestStatus(
            $request->get('request_vacation_id'),
            $request->get('response'),
            $request->get('request_statuses_id')
        );
        if ($request->get('request_statuses_id') == 1){ //if the request is approved
            $vacation = $this->getVacationRequestById($request->get('request_vacation_id'));
            $this->timeSheetService->setTimesheetsByLeader(
                $vacation->start,
                $vacation->end,
                $vacation->team_member_id,
                2);
        }
    }

}
