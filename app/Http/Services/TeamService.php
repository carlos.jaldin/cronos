<?php

namespace App\Http\Services;

use App\Models\Team;
use Illuminate\Support\Facades\Auth;

class TeamService
{
    private Team $team;
    //private Location $location;
    public function __construct(){
        $this->team = new Team();
        //$this->location = new Location();
    }

    public function getTeamMembers($id){
        try{
            return $this->team->getTeamMembers($id);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getTeamFromId($id){
        try{
            return $this->team->getTeamFromId($id);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function isLeaderOfTeam($id){
        try{
            return $this->team->isLeaderOfTeam($id, Auth::user()->id);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
