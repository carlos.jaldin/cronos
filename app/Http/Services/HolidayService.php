<?php

namespace App\Http\Services;

use App\Models\Holiday;

class HolidayService
{
    private Holiday $holiday;
    public function __construct(){
        $this->holiday = new Holiday();
    }

    public function getHolidaysByLocation($location)
    {
        try{
            //$resulado = $this->holiday->getHolidaysByLocation($location);
            //dd($resulado);
            return $this->holiday->getHolidaysByLocation($location);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
