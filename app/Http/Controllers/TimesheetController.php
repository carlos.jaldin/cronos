<?php

namespace App\Http\Controllers;

use App\Http\Services\TeamService;
use App\Http\Services\TimesheetService;
use App\Models\Timesheet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TimesheetController extends Controller
{
    private TimesheetService $timesheetService;
    private TeamService $teamService;
    public function __construct()
    {
        $this->timesheetService = new TimesheetService();
        $this->teamService = new TeamService();
    }

    public function setCheckIn($team){
        $this->timesheetService->setCheckIn($team);
        return redirect()->route('team.members', $team);
    }

    public function setCheckOut($timesheet,$team){
        $this->timesheetService->setCheckOut($timesheet);
        return redirect()->route('team.members', $team);
    }

    public function getUsrs($date1,$date2){
        $users = $this->timesheetService->showBetweenDates($date1,$date2);
        return view('teams.showTable') -> with (compact('users'));
    }

    public function setAbsence(Request $request, $team){

        $this->timesheetService->setTimesheetsByLeader(
            Carbon::createFromFormat('Y/m/d',  str_replace('-', '/',$request->get('start'))),
            Carbon::createFromFormat('Y/m/d',  str_replace('-', '/',$request->get('end'))),
            $request->get('member'),
            5);
        return redirect()->route('team.members', $team);
    }
    public function registerAbsence($member, $team){
        $team = $this->teamService->getTeamFromId($team);
        return view('timesheets.register')
                ->with(compact('member'))
                ->with(compact('team'));
    }
}
