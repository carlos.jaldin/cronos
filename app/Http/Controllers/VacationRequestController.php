<?php

namespace App\Http\Controllers;

use App\Http\Services\TeamService;
use App\Http\Services\VacationRequestService;
use Illuminate\Http\Request;

class VacationRequestController extends Controller
{
    private VacationRequestService $vacationRequestService;
    private TeamService $teamService;
    public function __construct(){
        $this->vacationRequestService = new VacationRequestService();
        $this->teamService = new TeamService();
    }

    public function create($teamId){
        $team = $this->teamService->getTeamFromId($teamId);

        return view('vacationRequests.register', compact('team'));
    }

    public function store(Request $request, $team){
        $this->vacationRequestService->createVacationRequest($request, $team);
        return redirect()->route('request.mines', $team);
    }

    public function mines($teamId){
		$team = $this->teamService->getTeamFromId($teamId);
		$requestVacations = $this->vacationRequestService->getUserVacationRequests($teamId);
        return view('vacationRequests.myRequests', compact('requestVacations', 'team'));
    }

	public function show($requestVacationId, $teamId) {
		$team = $this->teamService->getTeamFromId($teamId);
		$vacationRequest = $this->vacationRequestService->getVacationRequestById($requestVacationId);
		return view('vacationRequests.request', compact('vacationRequest', 'team'));
	}

    public function requestVacations($team){
        $requestVacations = $this->vacationRequestService->getRequestVacationsOfTeam($team);
        //dd($this->vacationRequestService->getRequestVacationsOfTeam($team));
        $team = $this->teamService->getTeamFromId($team);
        return view('vacationRequests.teamRequests')
                ->with(compact('requestVacations'))
                ->with(compact('team'));
    }

    public function registerResponse($requestId){
        $statuses = $this->vacationRequestService->allExceptOnHold();
        $request =  $this->vacationRequestService->getVacationRequestById($requestId);
        $member = $request->team_member_id;
        //dd($member);
        return view('vacationRequests.response')
                ->with(compact('statuses'))
                ->with(compact('request'))
                ->with(compact('member'));
    }

    public function setResponse(Request $request){
        $this->vacationRequestService->manageResponse($request);
        return redirect()->route('team.members', $request->get('team_id'));
    }
}
