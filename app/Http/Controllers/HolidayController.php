<?php

namespace App\Http\Controllers;

use App\Http\Services\HolidayService;
use Illuminate\Http\Request;

class HolidayController extends Controller
{
    private HolidayService $holidayService;

    public function __construct(){
        $this->holidayService = new HolidayService();
    }

    public function getHolidays($location){
        $locations = $this->holidayService->getHolidaysByLocation($location);
        return view('holidays.holidays') -> with (compact('locations'));
    }

}
