<?php

namespace App\Http\Controllers;

use App\Http\Services\LocationService;
use App\Http\Services\TimesheetService;
use App\Http\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private UserService $userService;
    private LocationService $locationService;
    private TimesheetService $timesheetService;
    public function __construct(){
        $this->userService = new UserService();
        $this->locationService = new LocationService();
        $this->timesheetService = new TimesheetService();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userService->listUser();
        return view('users.user', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = $this->locationService->getOnlyCities();
        //dd($locations);
        return view('auth.register', compact('locations'));
    }

    public function teams(){
        $teams = $this->userService->getTeamsOfUser(Auth::user()->id);
        return view('users.teams',compact('teams'));
    }

	public function history($date) {
		$month = new Carbon($date);
		$attendanceData = $this->timesheetService->getUserAttendanceHistoryByMonth(Auth::user()->id, $month);
		return view('users.attendanceHistory' )
				->with(compact('attendanceData'))
				->with(compact('date'));
	}

	public function historyPost(Request $request) {
		$date = $request->get('month');
		return redirect()->route('user.history', compact('date'));
	}

	public function attendanceOfDay($date) {
		$responses = $this->timesheetService->getTimeSheetsByDay(Auth::user()->id, $date);
		return view('users.attendanceOfDay')
			->with(compact('responses'))
			->with(compact('date'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
