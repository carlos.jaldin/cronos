<?php

namespace App\Http\Controllers;

use App\Http\Services\LocationService;
use App\Http\Services\TeamService;
use App\Http\Services\TimesheetService;
use App\Http\Services\UserService;
use App\Http\Services\HolidayService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Team;


class ReportController extends Controller
{

	private LocationService $locationService;
    private TeamService $teamService;
    private HolidayService $holidayService;
    private TimesheetService $timesheetService;
    private Team $team;

	public function __construct(){
        $this->locationService = new LocationService();
        $this->holidayService = new HolidayService();
        $this->teamService = new TeamService();
        $this->userService = new UserService();
        $this->timesheetService = new TimesheetService();
        $this->team = new Team();
    }

    public function indexRepHoliday($holidays=null){
        $_year = date("Y");
        $value = 1;
        $locations = $this->locationService->getOnlyCities();
        $holidays = $this->holidayService->getHolidaysByLocation($value);
        return view('report.reportHoliday')->with(compact('locations'))
                             ->with(compact('_year'))
                             ->with(compact('holidays'))
                             ->with(compact('value'));
    }

    public function getLocation(Request $request){
        $_year = date("Y");
        $value = $request->get('select_location');
        $locations = $this->locationService->getOnlyCities();
        $holidays = $this->holidayService->getHolidaysByLocation($value);
        return view('report.reportHoliday')->with(compact('locations'))
                             ->with(compact('_year'))
                             ->with(compact('holidays'))
                             ->with(compact('value'));
    }

    public function indexRepGlobal(){
        $_year = date("Y");
        $result = [];
        return view('report.reportGlobal')->with(compact('_year'))
                                          ->with(compact('result'));
    }

    public function getReportGlobal(Request $request){
       $_year = date("Y");
       $start_value =  $request->get('startDate');
       $end_value =  $request->get('endDate');
       ///usar las variables de start_value y end_value para generar el reporte segun las fechas
       $result = $this->timesheetService->showBetweenDates($start_value, $end_value);
       return view('report.reportGlobal')->with(compact('_year'))
                                         ->with(compact('result'));

    }

    public function indexRepTeamMember(){
        $_year = date("Y");
        $teams = $this->userService->getTeamsOfUser(Auth::user()->id);
        $value = null; 
        $members = [];
        return view('report.reportTeamMemer')->with(compact('_year'))
                                             ->with(compact('teams'))
                                             ->with(compact('value'))
                                             ->with(compact('members'));
    }

    public function getMembers(Request $request){
        $_year = date("Y");
        $teams = [];
        $value = $request->get('select_team');
        $members = $this->teamService->getTeamMembers($value);
        return view('report.reportTeamMemer')->with(compact('_year'))
                                             ->with(compact('teams'))
                                             ->with(compact('value'))
                                             ->with(compact('members'));
    }

}
