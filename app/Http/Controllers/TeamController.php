<?php

namespace App\Http\Controllers;

use App\Http\Services\TeamService;
use App\Http\Services\TimesheetService;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    private TeamService $teamService;
    private TimesheetService $timeSheetService;
    public function __construct()
    {
        $this->teamService = new TeamService();
        $this->timeSheetService = new TimesheetService();
    }

    public function getMembers($team){
        $members = $this->teamService->getTeamMembers($team);
        $teamdata = $this->teamService->getTeamFromId($team);
        $rest = $this->timeSheetService->getCheckInCheckOut($team);
        $checksinf = $rest['checks'];
        $checks = array(
            'checkIn' => ($rest['checkIn'] or $rest['new']),
            'checkOut' => ($rest['checkOut'])
        );
        $leader = $this->teamService->isLeaderOfTeam($team);
        return view('teams.members')
                ->with(compact('members'))
                ->with(compact('teamdata'))
                ->with(compact('checks'))
                ->with(compact('checksinf'))
                ->with(compact('leader'));
    }


}
