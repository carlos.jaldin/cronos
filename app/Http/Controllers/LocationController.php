<?php

namespace App\Http\Controllers;

use App\Http\Services\LocationService;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    private LocationService $locationService;

    public function __construct(){
        $this->locationService = new LocationService();
    }

    // public function getOnlyCities(){
    //     return $this->locationService->getOnlyCities();
    // }
}
