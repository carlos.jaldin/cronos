<?php

namespace App\Helpers;

class sortHelper
{
    public function merge_sort($my_array,$valueControl){
        if(count($my_array) == 1 ) return $my_array;
        $mid = count($my_array) / 2;
        $left = array_slice($my_array, 0, $mid);
        $right = array_slice($my_array, $mid);
        $left = $this->merge_sort($left,$valueControl);
        $right = $this->merge_sort($right,$valueControl);
        return  $this->merge($left, $right, $valueControl);
    }
    function merge($left, $right, $control){
        $res = array();
        while (count($left) > 0 && count($right) > 0){
            if($left[0][$control] > $right[0][$control]){
                $res[] = $right[0];
                $right = array_slice($right , 1);
            }else{
                $res[] = $left[0];
                $left = array_slice($left, 1);
            }
        }
        while (count($left) > 0){
            $res[] = $left[0];
            $left = array_slice($left, 1);
        }
        while (count($right) > 0){
            $res[] = $right[0];
            $right = array_slice($right, 1);
        }

        return $res;
    }
}
