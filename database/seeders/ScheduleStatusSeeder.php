<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ScheduleStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedule_statuses')->insert([
            [
                'status' => 'Vacation'
            ],
            [
                'status' => 'Holiday'
            ],
            [
                'status' => 'Sick leave'
            ],
            [
                'status' => 'Normal'
            ],
            [
                'status' => 'Absence'
            ],
        ]);
    }
}
