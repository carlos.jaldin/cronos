<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Carlos',
                'email' => 'jaldin@gmail.com',
                'password' => Hash::make('123'),
                'location_id' => 3
            ],
            [
                'name' => 'Andy',
                'email' => 'andy@gmail.com',
                'password' => Hash::make('123'),
                'location_id' => 2
            ],
            [
                'name' => 'Valeria',
                'email' => 'valeria@gmail.com',
                'password' => Hash::make('123'),
                'location_id' => 1
            ],
            [
                'name' => 'Angel',
                'email' => 'angel@gmail.com',
                'password' => Hash::make('123'),
                'location_id' => 1
            ],
            [
                'name' => 'Heidy',
                'email' => 'heidy@gmail.com',
                'password' => Hash::make('123'),
                'location_id' => 1
            ],
            [
                'name' => 'Rodrigo',
                'email' => 'rodrigo@gmail.com',
                'password' => Hash::make('123'),
                'location_id' => 1
            ],
            [
                'name' => 'Rosario',
                'email' => 'rosa@gmail.com',
                'password' => Hash::make('123'),
                'location_id' => 1
            ],
            [
                'name' => 'Marcelo',
                'email' => 'marcelo@gmail.com',
                'password' => Hash::make('123'),
                'location_id' => 1
            ],
        ]);
    }
}
