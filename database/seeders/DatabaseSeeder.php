<?php

namespace Database\Seeders;

use App\Models\TeamMember;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(TeamSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(ScheduleStatusSeeder::class);
        $this->call(HolidaySeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TeamMemberSeeder::class);
        $this->call(RequestStatusSeeder::class);
        //$this->call(VacationRequestSeeder::class);
        $this->call(TimesheetSeeder::class);
    }
}
