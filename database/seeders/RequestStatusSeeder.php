<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RequestStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('request_statuses')->insert([
            [
                'status' => 'Approved',
            ],
            [
                'status' => 'Reject',
            ],
            [
                'status' => 'On hold',
            ],
        ]);
    }
}
