<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimesheetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('timesheets')->insert([
            [
                'checkIn'=>'8:01:27',
                'checkOut'=>'15:23:29',
                'date'=>'2022-02-14',
                'schedule_status_id'=>'4',
                'team_member_id'=>'2',

            ],
            [
                'checkIn'=>'07:23:27',
                'checkOut'=>'13:10:29',
                'date'=>'2022-05-14',
                'schedule_status_id'=>'4',
                'team_member_id'=>'3',

            ],
            [
                'checkIn'=>'09:23:27',
                'checkOut'=>'16:23:29',
                'date'=>'2022-06-14',
                'schedule_status_id'=>'4',
                'team_member_id'=>'1',

            ],
            [
                'checkIn'=>'8:23:27',
                'checkOut'=>'12:23:29',
                'date'=>'2022-07-14',
                'schedule_status_id'=>'4',
                'team_member_id'=>'2',

            ],
            [
                'checkIn'=>'06:23:27',
                'checkOut'=>'12:23:29',
                'date'=>'2022-08-10',
                'schedule_status_id'=>'4',
                'team_member_id'=>'1',

            ],
            [
                'checkIn'=>'07:23:27',
                'checkOut'=>'11:23:29',
                'date'=>'2022-08-11',
                'schedule_status_id'=>'4',
                'team_member_id'=>'3',

            ],
            [
                'checkIn'=>'09:15:27',
                'checkOut'=>'15:23:29',
                'date'=>'2022-08-12',
                'schedule_status_id'=>'4',
                'team_member_id'=>'1',

            ],
            [
                'checkIn'=>'10:23:27',
                'checkOut'=>'15:23:29',
                'date'=>'2022-08-13',
                'schedule_status_id'=>'4',
                'team_member_id'=>'2',

            ],
            [
                'checkIn'=>'08:23:27',
                'checkOut'=>'15:23:29',
                'date'=>'2022-08-14',
                'schedule_status_id'=>'4',
                'team_member_id'=>'3',

            ],
            [
                'checkIn'=>'10:23:27',
                'checkOut'=>'16:23:29',
                'date'=>'2022-08-14',
                'schedule_status_id'=>'4',
                'team_member_id'=>'1',

            ],
        ]);
    }
}
