<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('team_members')->insert([
            [
                'initDate' => Carbon::parse('2022-07-16'),
                'isLeader' => true,
                'team_id' => 1,
                'user_id' => 1,
                'role_id' => 1
            ],
            [
                'initDate' => Carbon::parse('2022-07-16'),
                'isLeader' => false,
                'team_id' => 1,
                'user_id' => 2,
                'role_id' => 2
            ],
            [
                'initDate' => Carbon::parse('2022-06-16'),
                'isLeader' => true,
                'team_id' => 2,
                'user_id' => 3,
                'role_id' => 1
            ],
            [
                'initDate' => Carbon::parse('2022-06-16'),
                'isLeader' => true,
                'team_id' => 2,
                'user_id' => 2,
                'role_id' => 2
            ],
            [
                'initDate' => Carbon::parse('2022-08-12'),
                'isLeader' => true,
                'team_id' => 3,
                'user_id' => 4,
                'role_id' => 5
            ],
            [
                'initDate' => Carbon::parse('2022-08-12'),
                'isLeader' => false,
                'team_id' => 3,
                'user_id' => 5,
                'role_id' => 1
            ],
            [
                'initDate' => Carbon::parse('2022-08-13'),
                'isLeader' => false,
                'team_id' => 3,
                'user_id' => 6,
                'role_id' => 1
            ],
            [
                'initDate' => Carbon::parse('2022-08-12'),
                'isLeader' => true,
                'team_id' => 4,
                'user_id' => 4,
                'role_id' => 5
            ],
            [
                'initDate' => Carbon::parse('2022-08-12'),
                'isLeader' => false,
                'team_id' => 4,
                'user_id' => 7,
                'role_id' => 3
            ],
            [
                'initDate' => Carbon::parse('2022-08-13'),
                'isLeader' => false,
                'team_id' => 4,
                'user_id' => 8,
                'role_id' => 1
            ],
        ]);
    }
}
