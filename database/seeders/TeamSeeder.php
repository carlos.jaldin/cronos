<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            [
                "code" => "asj!@#SAdh",
                "name" => "Asadito con papa"
            ],
            [
                "code" => "mmDas@#31mas",
                "name" => "Majadito de charque"
            ],
            [
                "code" => "ioJn@#4dhhc",
                "name" => "Yuca con queso"
            ],
            [
                "code" => "hf5t@#30sfd",
                "name" => "Arroz con Huevo"
            ],
        ]);
    }
}
