<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->insert([
            [
                'abbreviation' => 'LP',
                'name' => 'la paz'
            ],
            [
                'abbreviation' => 'CBBA',
                'name' => 'Cochabamba'
            ],
            [
                'abbreviation' => 'SCZ',
                'name' => 'Santa cruz de la sierra'
            ],
            [
                'abbreviation' => 'BOL',
                'name' => 'Bolivia'
            ],
        ]);
    }
}
