<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class HolidaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('holidays')->insert([
            [
                'date' => Carbon::parse('2000-07-16'),
                'description' => 'Independencia de la paz',
                'isGlobal' => false,
                'location_id' => 1
            ],
            [
                'date' => Carbon::parse('2000-09-24'),
                'description' => 'Independencia de la santa cruz',
                'isGlobal' => false,
                'location_id' => 3
            ],
            [
                'date' => Carbon::parse('2000-08-06'),
                'description' => 'Independencia de Bolvia',
                'isGlobal' => true,
                'location_id' => 4
            ],
        ]);
    }
}
