<?php

use App\Http\Controllers\TeamController;
use App\Http\Controllers\TimesheetController;
use App\Http\Controllers\VacationRequestController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HolidayController;
use App\Http\Controllers\ReportController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// /user/all
// /user/data
Route::group(['prefix' => 'user'], function () {
    Route::get('all', [UserController::class, 'index'])->name('user.all');
    Route::get('create', [UserController::class, 'create'])->name('user.create');
    Route::get('teams', [UserController::class, 'teams'])->name('user.teams');
	Route::get('history/{date}', [UserController::class, 'history'])->name('user.history');
	Route::get('attendanceOfDay/{date}', [UserController::class, 'attendanceOfDay'])->name('user.attendanceOfDay');
	Route::post('historyPost', [UserController::class, 'historyPost'])->name('user.historyPost');
});

Route::group(['prefix' => 'team'], function () {
    Route::get('members/{team}', [TeamController::class, 'getMembers'])->name('team.members');
});

Route::group(['prefix' => 'timesheet'], function () {
    Route::get('checkIn/{team}', [TimesheetController::class, 'setCheckIn'])->name('timesheet.checkIn');
    Route::get('checkOut/{timesheet}/{team}', [TimesheetController::class, 'setCheckOut'])->name('timesheet.checkOut');
    Route::post('absence/{team}', [TimesheetController::class, 'setAbsence'])->name('timesheet.absence');
    Route::get('register/absence/{member}/{team}', [TimesheetController::class, 'registerAbsence'])->name('timesheet.registerAbsence');
});

Route::group(['prefix' => 'request'], function () {
    Route::get('mines/{team}', [VacationRequestController::class, 'mines'])->name('request.mines');
    Route::get('create/{team}', [VacationRequestController::class, 'create'])->name('request.create');
	Route::get('show/{request}/{team}', [VacationRequestController::class, 'show'])->name('request.show');
    Route::post('store/{team}', [VacationRequestController::class, 'store'])->name('request.store');
    Route::get('vacations/{team}', [VacationRequestController::class, 'requestVacations'])->name('request.vacations');
    Route::get('register/{requestId}', [VacationRequestController::class, 'registerResponse'])->name('request.register');
    Route::post('response', [VacationRequestController::class, 'setResponse'])->name('request.response');
});

Route::group(['prefix' => 'report'], function () {
    Route::get('holiday', [ReportController::class, 'indexRepHoliday'])->name('report.holiday');
    Route::post('location', [ReportController::class, 'getLocation'])->name('report.location');
    Route::get('global', [ReportController::class, 'indexRepGlobal'])->name('report.global');
    Route::post('detail', [ReportController::class, 'getReportGlobal'])->name('report.detail');
    Route::get('team', [ReportController::class, 'indexRepTeamMember'])->name('report.team');
    Route::post('member', [ReportController::class, 'getMembers'])->name('report.member');
});


Route::group(['prefix' => 'holidays'], function () {
    Route::get('location/{location_id}', [HolidayController::class, 'getHolidays'])->name('holiday.holidays');
});
