@extends('layouts.app')
@section('content')
    <div class="container">
		<div class="row">
			<ul class="col-6">
				<li class="list-group-item"><h2>Teams</h2></li>
			</ul>
			<div class="col-6 text-end">
				<a class="btn btn-primary" href="{{route('user.history', Carbon\Carbon::now()->format('Y-m'))}}">Attendance History</a>	
			</div>
		</div>
		
		<div class="card">
			<table class="table table-striped">
				<thead>
				<th>Id</th>
				<th>Code</th>
				<th>Name</th>
				<th>Options</th>
				</thead>
				<tbody>
				@foreach ($teams as $team)
					<tr>
						<td>{{$team->id}}</td>
						<td>{{$team->code}}</td>
						<td>{{$team->name}}</td>
						<td>
							<a href="{{route('team.members', $team->id)}}"><button type="button" class="btn btn-primary btn-sm" >Show</button></a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
        
        {{--        <div class="table table-striped">{{$users->links()}}</div>--}}
    </div>
@endsection
