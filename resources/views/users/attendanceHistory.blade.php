@extends('layouts.app')

@section('content')
	<div class="container">
		<h3>Attendance History</h3>
		<form method="POST" action="{{route('user.historyPost')}}" class="row mb-3">
			@csrf
			<div class="row col-8">
				<label for="month" class="col-2 col-form-label">Select a month</label>

				<div class="col-10">
					<input id="month" type="month" class="form-control" name="month" value="{{ $date }}" autocomplete="start">
				</div>
			</div>

			<div class="col-4">
				<button class="btn btn-primary" type="submit">
					send
				</button>
			</div>
		</form>
		<div class="card">
			<table class="table table-striped">
				<thead class="thead-light">
					<th scope="col">Date</th>
					<th scope="col">Team</th>
					<th scope="col">Hours</th>
					<th scope="col">Status</th>
					<th scope="col">Options</th>
				</thead>
				<tbody>
					@if (sizeof($attendanceData) > 0)
						@foreach ($attendanceData as $attendance)
							<tr>
								<td>{{$attendance['date']}}</td>
								<td>{{$attendance['team']}}</td>
								<td>{{$attendance['hours']}}</td>
								<td>{{$attendance['timesheetstatus']}}</td>
								<td>
									<a href="{{route('user.attendanceOfDay', $attendance['date'])}}" class="btn btn-primary btn-sm">Show</a>
								</td>
							</tr>
						@endforeach
					@else
						<tr>
							<td>You don't have any history for this month yet</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
@endsection
