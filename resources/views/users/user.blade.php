@extends('layouts.app')
@section('content')
    <h1>Hola Andy</h1>

    <div class="container">
        <br>
        <br>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><h2>usuarios</h2></li>
        </ul>
        <br>
        <table class="table table-striped">
            <thead>
            <th>id</th>
            <th>Nombre</th>
            <th>email</th>
            <th>location</th>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->location->name}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
{{--        <div class="table table-striped">{{$users->links()}}</div>--}}
    </div>
@endsection
