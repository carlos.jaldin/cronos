@extends('layouts.app')

@section('content')
	<div class="container">
		<h1>{{$date}}</h1>
		<div class="card">
			<table class="table table-striped">
				<thead class="thead-light">
					<th scope="col">Team Name</th>
					<th scope="col">Role</th>
					<th scope="col">Status</th>
					<th scope="col">Check In</th>
					<th scope="col">Check Out</th>
				</thead>
				<tbody>
					@if (sizeof($responses) > 0)
						@foreach ($responses as $response)
							<tr>
								<td>{{$response['teamName']}}</td>
								<td>{{$response['role']}}</td>
								<td>{{$response['timesheetstatus']}}</td>
								<td>{{$response['checkIn'] ? $response['checkIn'] : 'Not checked in'}}</td>
								<td>{{$response['checkOut'] ? $response['checkOut'] : 'Not checked out'}}</td>
							</tr>
						@endforeach
					@else
						<tr>
							<td>Data not found</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
@endsection