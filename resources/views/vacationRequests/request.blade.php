@extends('layouts.app')

@section('content')

<div class="container">
	<div class="card">
		<div class="card-body p-5 pt-3">
			<div class="">
				<h2>{{ __('Vacation Request') }}</h2>
			</div>
			<div class="row">
				<div class="col-3 fw-bold">
					Team:
				</div>
				<div class="col-9">
					{{$team->name}}
				</div>
			</div>
			<div class="row">
				<div class="col-3 fw-bold">
					Id:
				</div>
				<div class="col-9">
					{{$vacationRequest->id}}
				</div>
			</div>
			<div class="row">
				<div class="col-3 fw-bold">
					Start:
				</div>
				<div class="col-9">
					{{$vacationRequest->start->format('Y-m-d')}}
				</div>
			</div>
			<div class="row">
				<div class="col-3 fw-bold">
					End:
				</div>
				<div class="col-9">
					{{$vacationRequest->end->format('Y-m-d')}}
				</div>
			</div>
			<div class="row">
				<div class="col-3 fw-bold">
					Viewed:
				</div>
				<div class="col-9">
					@if ($vacationRequest->viewed == 0)
						<span class="badge bg-secondary rounded-pill">not viewed yet</span>
					@elseif ($vacationRequest->viewed == 1)
						<span class="badge bg-success rounded-pill">viewed</span>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-3 fw-bold">
					Response:
				</div>
				<div class="col-9">
					{{$vacationRequest->response}}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
