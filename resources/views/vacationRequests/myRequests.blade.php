@extends('layouts.app')

@section('content')
    <div class="container">
		<div class="row">
			<div class="col-6">
				<h1>Vacation Requests</h1>
			</div>
			<div class="col-6 text-end pt-3 pe-3 fs-5">
				Team: {{$team->name}}
			</div>
		</div>
		<div class="container card">
			<table class="table table-striped">
				<thead class="thead-light">
					<th scope="col">Start</th>
					<th scope="col">End</th>
                    <th scope="col">Response</th>
					<th scope="col">viewed</th>
                    <th scope="col">state</th>
					<th scope="col">Options</th>
				</thead>
				<tbody>
					@if (sizeof($requestVacations) > 0)
						@foreach ($requestVacations as $requestVacation)
							<tr>
								<td>{{$requestVacation->start->toFormattedDateString()}}</td>
								<td>{{$requestVacation->end->toFormattedDateString()}}</td>
                                <td>{{($requestVacation->response)==null?"not answered yer":"$requestVacation->response"}}</td>
								@if ($requestVacation->viewed == 0)
									<td>
										<span class="badge bg-secondary rounded-pill">not viewed</span>
									</td>
								@else ($requestVacation->viewed == 1)
									<td>
										<span class="badge bg-success rounded-pill">viewed</span>
									</td>
								@endif
                                <td>
                                    @if ($requestVacation->requestStatuses->id == 1)
                                        <span class="badge bg-success rounded-pill">{{$requestVacation->requestStatuses->status}}</span>
                                    @elseif ($requestVacation->requestStatuses->id == 2)
                                        <span class="badge bg-danger rounded-pill">{{$requestVacation->requestStatuses->status}}</span>
                                    @else
                                        <span class="badge bg-secondary rounded-pill">{{$requestVacation->requestStatuses->status}}</span>
                                    @endif
                                </td>
								<td>
									<a href="{{route('request.show', [$requestVacation->id, $team->id])}}" class="btn btn-primary btn-sm">Show</a>
								</td>
							</tr>
						@endforeach
					@else
						<tr>
							<td>
								You don't have any vacation request
							</td>
						</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
@endsection
