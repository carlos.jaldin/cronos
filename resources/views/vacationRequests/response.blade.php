@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <h3>
{{--                @dump($request)--}}
                {{"vacation request from ".
                    $request->start->toFormattedDateString().
                    " to ".
                    $request->end->toFormattedDateString()
                }}
            </h3>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Register') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('request.response') }}">
                            @csrf

                            <div class="form-group row mb-3">
                                <label for="request_statuses_id" class="col-md-4 col-form-label text-md-end">{{ __('Estado') }}</label>
                                <div class="col-md-6">
                                    <select name="request_statuses_id" id="request_statuses_id" class="form-select form-control @error('request_statuses_id') is-invalid @enderror">
                                        <option value="">--Select status--</option>
                                        @foreach ($statuses as $status)
                                            <option value="{{$status->id}}">{{$status->status}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="response" class="col-md-4 col-form-label text-md-end">{{ __('Response') }}</label>

                                <div class="col-md-6">
                                    <input id="response" type="text" class="form-control @error('response') is-invalid @enderror" name="response" value="{{ old('response') }}" autocomplete="response" autofocus>
                                </div>
                            </div>

                            @if ($errors->count() > 0)
                                <div class="container w-75 alert alert-danger">
                                    <ul class="">
                                        @foreach ($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <input type="hidden" name="request_vacation_id" value="{{$request->id}}">
                            <input type="hidden" name="team_member_id" value="{{$request->team_member_id}}">
                            <input type="hidden" name="team_id" value="{{$request->teamMember->team_id}}">
                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
