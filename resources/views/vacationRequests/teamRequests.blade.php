@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1>Team vacation requests on hold</h1>
            </div>
            <div class="col-6 text-end pt-3 pe-3 fs-5">
                Team: {{$team->name}}
            </div>
        </div>
        <div class="container card">
            <table class="table table-striped">
                <thead class="thead-light">
                <th scope="col">Member name</th>
                <th scope="col">Member email</th>
                <th scope="col">Start</th>
                <th scope="col">End</th>
                <th scope="col">viewed</th>
                <th scope="col">Options</th>
                </thead>
                <tbody>
{{--                @dump($requestVacations)--}}
                @if (sizeof($requestVacations) > 0)
                    @foreach ($requestVacations as $requestVacation)
                        <tr>
                            <td>{{$requestVacation->teamMember->user->name}}</td>
                            <td>{{$requestVacation->teamMember->user->email}}</td>
                            <td>{{$requestVacation->start->toFormattedDateString()}}</td>
                            <td>{{$requestVacation->end->toFormattedDateString()}}</td>
                            @if ($requestVacation->viewed == 0)
                                <td>
                                    <span class="badge bg-secondary rounded-pill">not viewed</span>
                                </td>
                            @else ($requestVacation->viewed == 1)
                                <td>
                                    <span class="badge bg-success rounded-pill">approved</span>
                                </td>
                            @endif
                            <td>
{{--                                <a href="{{route('request.show', [$requestVacation->id, $team->id])}}" class="btn btn-primary btn-sm">Show</a>--}}
                                <a href="{{route('request.register',$requestVacation->id)}}" class="btn btn-info btn-sm">response</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>
                            You don't have any vacation request
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
