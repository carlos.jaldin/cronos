@extends('layouts.app')

@section('content')

    <table class="table table-striped">
        <thead>
        <th>Fecha</th>
        <th>Descripcion</th>
        <th>Nacional</th>
        <th>Lugar</th>
        </thead>
        <tbody>
        @foreach ($locations as $location)
            <tr>
                <td>{{$location->date->format('m-d')}}</td>
                <td>{{$location->description}}</td>
                <td>{{$location->isGlobal}}</td>
                <td>{{$location->location->name}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <h1>Aqui hacen un listado de los holidays por location</h1>
@endsection
