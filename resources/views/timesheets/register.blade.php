@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
						<h5>{{"Request vacation for the team ".$team->name }}</h5>
					</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('timesheet.absence', $team->id) }}">
                            @csrf

                            <div class="row mb-3">
                                <label for="start" class="col-md-4 col-form-label text-md-end">{{ __('Start Date') }}</label>

                                <div class="col-md-6">
                                    <input id="start" type="date" class="form-control @error('end') is-invalid @enderror" name="start" value="{{ old('start') }}"  autocomplete="start" min="{{\Carbon\Carbon::now('America/La_Paz')->format('Y-m-d')}}" max="2100-01-01">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="end" class="col-md-4 col-form-label text-md-end">{{ __('End Date') }}</label>

                                <div class="col-md-6">
                                    <input id="end" type="date" class="form-control @error('end') is-invalid @enderror" name="end" value="{{ old('end') }}"  autocomplete="end">
                                </div>
                            </div>

							@if ($errors->count() > 0)
								<div class="container w-75 alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{$error}}</li>
										@endforeach
									</ul>
								</div>
							@endif
                            <input type="hidden" name="member" value="{{$member}}">
                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
