@extends('layouts.app')
@section('content')
    <div class="container">
        <br>
        <ul class="list-group list-group-flush">
            <br>
            <li class="list-group-item"><h2>{{'Team members of team '.$teamdata->name}}</h2></li>
{{--             <li class="list-group-item">@dump($checksinf)</h2></li>--}}
			<div class="row">

				<div class="col-9">
                    <br>
					{{-- Si de pecho no existe una tupla en esta fecha, mostrar el solicitar vacacion --}}
					{{-- Mandar a formulario donde se podra poner de que fecha a que fecha sera la vacacion
					y redireccionar a ver mis vacaciones --}}
					<a href="{{route('request.create',$teamdata->id)}}" type="button" class="btn btn-primary">Request Vacation</a>
					<a href="{{route('request.mines',$teamdata->id)}}" type="button" class="btn btn-warning">My requests List</a>
{{--					<a href="{{ route('team.members', $teamdata->id) }}" type="button" class="btn btn-primary">Set schedule status</a>--}}
                    @if($leader)
                        <a href="{{route('request.vacations',$teamdata->id)}}" type="button" class="btn btn-success">Team members vacation requests</a>

                    @endif
				</div>
				<div class="col-3 text-end">
					@if ($checks['checkIn'])
					{{-- mostrar Si no existe una tupla en esta fecha que tenga el checkOut null --}}
					<a href="{{route('timesheet.checkIn', $teamdata->id)}}" type="button" class="btn btn-success">Check In</a>
					@endif
					@if ($checks['checkOut'])
					{{-- mostrar Si existe una tupla en esta fecha con el CheckOut null --}}
					<a href="{{ route('timesheet.checkOut',['timesheet' => $checksinf->id,'team' => $teamdata->id])}}" type="button" class="btn btn-danger">Check Out</a>
					@endif
				</div>
			</div>
        </ul>
        <br>
		<div class="card">
			<table class="table table-striped">
				<thead>
				<th>Id</th>
				<th>Name</th>
				<th>Email</th>
				<th>Location</th>
                @if($leader)
                <th>Options</th>
                @endif
				</thead>
				<tbody>
				@foreach ($members as $member)
					<tr>
						<td>{{$member->id}}</td>
						<td>{{$member->name}}</td>
						<td>{{$member->email}}</td>
						<td>{{$member->location->name}}</td>
                        @if($leader)
                        <td><a href="{{route('timesheet.registerAbsence',['member' => $member->id,'team' => $teamdata->id])}}" type="button" class="btn btn-danger">Set Absence</a></td>
                        @endif
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
        {{--        <div class="table table-striped">{{$users->links()}}</div>--}}
    </div>
@endsection
