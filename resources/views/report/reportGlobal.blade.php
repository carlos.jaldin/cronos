@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h1>Report {{ $_year }}</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="team_general-tab" data-bs-toggle="tab"
                                    data-bs-target="#team_general" type="button" role="tab"
                                    aria-controls="team_general" aria-selected="false">Report General</button>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="team_general" role="tabpanel"
                                aria-labelledby="team_general-tab">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <form name="add-blog-post-form" id="add-blog-post-form" method="POST"
                                                action="{{ route('report.detail') }}">
                                                @csrf
                                                <br>
                                                <div class="form-group col-lg-6">
                                                    <label class="control-label">Start Date:</label>
                                                    <input id="startDate" name="startDate" type="date"
                                                        class="form-control" value="<?php echo date('Y-m-d'); ?>" max="<?php echo date('Y-m-d'); ?>">
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label class="control-label">End Date:</label>
                                                    <input id="endDate" name="endDate" type="date" class="form-control"
                                                        value="<?php echo date('Y-m-d'); ?>" max="<?php echo date('Y-m-d'); ?>">
                                                </div>
                                                <br>
                                                <button type="submit" class="btn btn-success">Show Report</button>
                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <br>
                                            <table class="table table-striped">
                                                <thead>
                                                    <th>Name Employee</th>
                                                    <th>Role</th>
                                                    <th>Location</th>
                                                    <th>Team</th>
                                                    <th>Worked hours</th>
                                                    <th>Date</th>

                                                </thead>
                                                <tbody>

                                                @if(sizeof($result) > 0)

                                                    @foreach ($result as $data)

                                                        <tr>
                                                            <td>{{ $data['username'] }}</td>
                                                            <td>{{ $data['role'] }}</td>
                                                            <td>{{ $data['location'] }}</td>
                                                            <td>{{ $data['team'] }}</td>
                                                            <td>{{ $data['hours'] }}</td>
                                                            <td>{{ $data['date'] }}</td>
                                                                                           </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
