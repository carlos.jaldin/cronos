@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="page-header">
            <h1>Report Team Members {{ $_year }}</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="team_member" data-bs-toggle="tab"
                                    data-bs-target="#team_member" type="button" role="tab" aria-controls="team_member"
                                    aria-selected="true">Team Members</button>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="team_member" role="tabpanel"
                                aria-labelledby="team_member">
                                <div class="container">
                                    <div class="row">
                                        <br>
                                        <div class="col-md-5">
                                            <br>
                                            <form name="add-blog-post-form" id="add-blog-post-form" method="POST"
                                                action="{{ route('report.member') }}">
                                                @csrf
                                                <div class="form-group col-md-12">
                                                    <div class="col-md-8">
                                                        <label class="control-label">Teams:</label>
                                                        <select class="form-select" aria-label="Default select example"
                                                            name="select_team">
                                                            @foreach ($teams as $team)
                                                                <option value="{{ $team->id }}"
                                                                    {{ ($value != null ? $value : 100) == $team->id ? 'selected' : '' }}>
                                                                    {{ $team->name }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-4">
                                                        <button type="submit" class="btn btn-success">GET MEMBERS</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <br>
                                            <table class="table table-striped">
                                                <thead>
                                                    <th>Name Employee</th>
                                                    <th>Email</th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($members as $member)
                                                        <tr>
                                                            <td>{{ $member->name }}</td>
                                                            <td>{{ $member->email }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
