@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="page-header">
            <h1>Report Holiday's  {{ $_year }}</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="holiday-tab" data-bs-toggle="tab"
                                    data-bs-target="#holiday" type="button" role="tab" aria-controls="holiday"
                                    aria-selected="true">Holiday</button>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="holiday" role="tabpanel"
                                aria-labelledby="holiday-tab">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <br>
                                            <form name="add-blog-post-form" id="add-blog-post-form" method="POST"
                                                action="{{ route('report.location') }}">
                                                @csrf
                                                <div class="form-group col-md-12">
                                                    <div class="col-md-8">
                                                        <label class="control-label">Country:</label>
                                                        <select class="form-select" aria-label="Default select example"
                                                            name="select_location">
                                                            @foreach ($locations as $bc)
                                                                <option value="{{ $bc->id }}"
                                                                    {{ ($value != null ? $value : 100) == $bc->id ? 'selected' : '' }}>
                                                                    {{ $bc->name }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <button type="submit" class="btn btn-success">Show Report</button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <br>
                                            <table class="table table-striped">
                                                <thead>
                                                    <th>#</th>
                                                    <th>Date</th>
                                                    <th>Description</th>
                                                    <th>City</th>
                                                </thead>
                                                <tbody>
                                                    @if ($holidays != null)
                                                        @foreach ($holidays as $holiday)
                                                            <tr>
                                                                <td>{{ $holiday->date->format('m-d') }}</td>
                                                                <td>{{ $holiday->description }}</td>
                                                                <td>{{ $holiday->isGlobal }}</td>
                                                                <td>{{ $holiday->location->name }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection